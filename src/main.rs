use clap::Parser;
use facts::Facts;
use std::{fs, io, path::Path, process::Command};

mod facts;

const OUT_DIR: &str = ".dwad";

/// Datalog-based WebAssembly decompiler
#[derive(Debug, Parser)]
struct Args {
    /// WebAssembly file to decompile
    input_file: String,

    /// Number of CPU cores to use
    #[clap(short, long, default_value = "1")]
    num_cores: String,

    /// File to write the decompiled program to
    #[clap(short, long, default_value = "out.c")]
    out_file: String,
}

fn main() -> walrus::Result<()> {
    let args = Args::parse();
    fs::remove_dir_all(OUT_DIR).ok();
    fs::create_dir_all(OUT_DIR)?;
    Facts::generate(args.input_file)?;
    generate_code(args.out_file, args.num_cores)?;
    fs::remove_dir_all(OUT_DIR)?;
    Ok(())
}

fn generate_code(out_file: impl AsRef<Path>, num_cores: String) -> io::Result<()> {
    Command::new("souffle")
        .arg("src/datalog/main.dl")
        .arg("-F")
        .arg(OUT_DIR)
        .arg("-D")
        .arg(OUT_DIR)
        .arg("-j")
        .arg(num_cores)
        .output()?;
    fs::rename(format!("{OUT_DIR}/CFunction.csv"), out_file)?;
    Ok(())
}
