use clap::Parser;
use std::{
    fs::{self, File},
    io,
    process::{Command, Stdio},
};

const OUT_DIR: &str = ".dwad-eval";

/// dwad evaluator
#[derive(Debug, Parser)]
#[clap(name = "dwad-eval")]
struct Args {
    /// Number of programs to decompile
    num_programs: usize,

    /// Number of test cases per program
    num_test_cases: usize,

    /// Number of CPU cores to use
    #[clap(short, long, default_value = "1")]
    num_cores: String,

    /// Directory to read programs from
    #[clap(short, long, default_value = "gen")]
    in_dir: String,
}

fn main() -> io::Result<()> {
    let args = Args::parse();
    fs::remove_dir_all(OUT_DIR).ok();
    Command::new("cp")
        .arg("-r")
        .arg(args.in_dir)
        .arg(OUT_DIR)
        .output()?;
    let mut total_tests_passed = 0;
    let mut failed_to_decompile = 0;
    let mut failed_to_recompile = 0;
    let mut passed_all = 0;
    let mut passed_some = 0;
    let mut passed_none = 0;
    let mut total_lines = 0;
    let mut total_line_diff = 0;
    for i in 0..args.num_programs {
        println!("program{i}:");
        println!(
            "gcc compilation finished with {}",
            Command::new("gcc")
                .arg("-O1")
                .arg("-o")
                .arg(format!("{OUT_DIR}/program{i}"))
                .arg(format!("{OUT_DIR}/program{i}_main.c"))
                .output()?
                .status
        );
        println!(
            "emcc compilation finished with {}",
            Command::new("emcc")
                .arg("-O1")
                .arg("-g")
                .arg("-s")
                .arg("SIDE_MODULE=1")
                .arg("-o")
                .arg(format!("{OUT_DIR}/program{i}.wasm"))
                .arg(format!("{OUT_DIR}/program{i}.c"))
                .output()?
                .status
        );
        let decompile_status = Command::new("timeout")
            .arg("1m")
            .arg("target/release/dwad")
            .arg("-n")
            .arg(&args.num_cores)
            .arg("-o")
            .arg(format!("{OUT_DIR}/program{i}.c"))
            .arg(format!("{OUT_DIR}/program{i}.wasm"))
            .output()?
            .status;
        println!("dwad decompilation finished with {decompile_status}");
        if !decompile_status.success() {
            failed_to_decompile += 1;
            continue;
        }
        let recompile_status = Command::new("gcc")
            .arg("-O1")
            .arg("-o")
            .arg(format!("{OUT_DIR}/program{i}_dwad"))
            .arg(format!("{OUT_DIR}/program{i}_main.c"))
            .output()?
            .status;
        println!("gcc recompilation finished with {recompile_status}");
        if !recompile_status.success() {
            failed_to_recompile += 1;
            continue;
        }
        let mut tests_passed = 0;
        for j in 0..args.num_test_cases {
            let output = Command::new(format!("{OUT_DIR}/program{i}"))
                .arg(j.to_string())
                .output()?;
            let output_dwad = Command::new("timeout")
                .arg("1s")
                .arg(format!("{OUT_DIR}/program{i}_dwad"))
                .arg(j.to_string())
                .output()?;
            if output == output_dwad {
                tests_passed += 1;
            }
        }
        total_tests_passed += tests_passed;
        println!("Tests passed: {tests_passed}/{}", args.num_test_cases);
        if tests_passed == args.num_test_cases {
            passed_all += 1;
        } else if tests_passed != 0 {
            passed_some += 1;
        } else {
            passed_none += 1;
        }
        let emcc_recompile_status = Command::new("emcc")
            .arg("-O1")
            .arg("-g")
            .arg("-s")
            .arg("SIDE_MODULE=1")
            .arg("-o")
            .arg(format!("{OUT_DIR}/program{i}_dwad.wasm"))
            .arg(format!("{OUT_DIR}/program{i}.c"))
            .output()?
            .status;
        println!("emcc recompilation finished with {emcc_recompile_status}");
        if emcc_recompile_status.success() {
            Command::new("sed")
                .arg("/^\\s*;;.*$/d")
                .stdin(
                    Command::new("wasm-dis")
                        .arg(format!("{OUT_DIR}/program{i}.wasm"))
                        .stdout(Stdio::piped())
                        .spawn()?
                        .stdout
                        .unwrap(),
                )
                .stdout(File::create(format!("{OUT_DIR}/program{i}.wat"))?)
                .spawn()?
                .wait()?;
            Command::new("sed")
                .arg("/^\\s*;;.*$/d")
                .stdin(
                    Command::new("wasm-dis")
                        .arg(format!("{OUT_DIR}/program{i}_dwad.wasm"))
                        .stdout(Stdio::piped())
                        .spawn()?
                        .stdout
                        .unwrap(),
                )
                .stdout(File::create(format!("{OUT_DIR}/program{i}_dwad.wat"))?)
                .spawn()?
                .wait()?;
            let lines = String::from_utf8(
                Command::new("wc")
                    .arg("-l")
                    .stdin(File::open(format!("{OUT_DIR}/program{i}.wat"))?)
                    .output()?
                    .stdout,
            )
            .unwrap()
            .trim()
            .parse::<u32>()
            .unwrap();
            let line_diff = String::from_utf8(
                Command::new("wc")
                    .arg("-l")
                    .stdin(
                        Command::new("diff")
                            .arg("-y")
                            .arg("--suppress-common-lines")
                            .arg(format!("{OUT_DIR}/program{i}.wat"))
                            .arg(format!("{OUT_DIR}/program{i}_dwad.wat"))
                            .stdout(Stdio::piped())
                            .spawn()?
                            .stdout
                            .unwrap(),
                    )
                    .output()?
                    .stdout,
            )
            .unwrap()
            .trim()
            .parse::<u32>()
            .unwrap();
            total_lines += lines;
            total_line_diff += line_diff;
            println!("Line diff: {line_diff}/{lines}");
        }
        println!();
    }
    println!("{failed_to_decompile} programs could not be decompiled");
    println!("{failed_to_recompile} programs could not be recompiled");
    println!("{passed_all} programs passed all tests");
    println!("{passed_some} programs passed some tests");
    println!("{passed_none} programs passed no tests");
    println!(
        "Total tests passed: {total_tests_passed}/{}",
        args.num_programs * args.num_test_cases
    );
    println!("Total line diff: {total_line_diff}/{}", total_lines);
    fs::remove_dir_all(OUT_DIR)?;
    Ok(())
}
