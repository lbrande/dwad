use clap::{ArgEnum, Parser};
use lib::{GenOptions, Program};
use rand::prelude::*;
use std::{
    fs::{self},
    io::{self},
};

mod lib;

const COMPLEXITY_OPTIONS: [GenOptions; 5] = [
    GenOptions {
        function_count: 1,
        param_count_range: 1..=2,
        block_size_range: 3..=4,
        max_stmt_depth: 0,
        max_expr_depth: 2,
    },
    GenOptions {
        function_count: 2,
        param_count_range: 1..=2,
        block_size_range: 3..=4,
        max_stmt_depth: 0,
        max_expr_depth: 2,
    },
    GenOptions {
        function_count: 1,
        param_count_range: 1..=2,
        block_size_range: 3..=4,
        max_stmt_depth: 1,
        max_expr_depth: 2,
    },
    GenOptions {
        function_count: 2,
        param_count_range: 1..=2,
        block_size_range: 3..=4,
        max_stmt_depth: 1,
        max_expr_depth: 2,
    },
    GenOptions {
        function_count: 1,
        param_count_range: 2..=3,
        block_size_range: 3..=4,
        max_stmt_depth: 1,
        max_expr_depth: 3,
    },
];

/// C code generator
#[derive(Debug, Parser)]
#[clap(name = "code-gen")]
struct Args {
    /// Number of programs to generate
    num_programs: usize,

    /// Complexity of generated programs
    #[clap(arg_enum)]
    complexity: CodeComplexity,

    /// Directory to write generated programs to
    #[clap(short, long, default_value = "gen")]
    out_dir: String,
}

#[derive(ArgEnum, Clone, Debug)]
enum CodeComplexity {
    Low,
    Low2,
    Normal,
    Normal2,
    High,
}

fn main() -> io::Result<()> {
    let args = Args::parse();
    fs::remove_dir_all(&args.out_dir).ok();
    fs::create_dir_all(&args.out_dir)?;
    let options = &COMPLEXITY_OPTIONS[args.complexity as usize];
    let mut rng = thread_rng();
    for i in 0..args.num_programs {
        Program::generate(format!("program{}", i), &args.out_dir, options, &mut rng)?;
    }
    Ok(())
}
