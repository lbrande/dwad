use rand::prelude::*;
use std::{
    fmt::{self, Display, Formatter},
    fs, io,
    ops::RangeInclusive,
};

const MAX_TRIES: u32 = 10;

#[derive(Debug)]
pub(crate) struct Program {
    name: String,
    includes: Vec<String>,
    functions: Vec<Function>,
}

impl Display for Program {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}",
            includes_to_string(&self.includes),
            functions_to_string(&self.functions)
        )
    }
}

impl Program {
    pub(crate) fn generate(
        name: String,
        out_dir: &str,
        options: &GenOptions,
        rng: &mut ThreadRng,
    ) -> io::Result<()> {
        let program = Program::generate_program(name.to_string(), options, rng);
        fs::write(
            format!("{out_dir}/{name}.c"),
            program.to_string().as_bytes(),
        )?;
        let main = program.generate_main();
        fs::write(
            format!("{out_dir}/{}.c", main.name),
            main.to_string().as_bytes(),
        )?;
        Ok(())
    }

    fn generate_program(name: String, options: &GenOptions, rng: &mut ThreadRng) -> Program {
        let mut functions = Vec::new();
        while functions.len() < options.function_count {
            let mut decl = FunctionDecl {
                name: format!("function{}", functions.len()),
                params: (0..rng.gen_range(options.param_count_range.clone()))
                    .map(|i| (Type::generate(1, rng), format!("param{i}")))
                    .collect(),
                return_type: Type::generate(0, rng),
            };
            let vars = decl.params.clone();
            let body = StmtsGen::generate(
                &mut decl,
                &vars,
                &functions
                    .iter()
                    .map(|function: &Function| function.decl.clone())
                    .collect::<Vec<_>>(),
                options,
                rng,
            );
            functions.push(Function { decl, body })
        }
        Program {
            name,
            includes: Vec::new(),
            functions,
        }
    }

    fn generate_main(&self) -> Program {
        let decl = FunctionDecl {
            name: "main".to_string(),
            params: vec![
                (
                    Type::from_base_type(BaseType::Int(true)),
                    "argc".to_string(),
                ),
                (
                    Type {
                        base_type: BaseType::Char(true),
                        pointer_depth: 2,
                    },
                    "argv".to_string(),
                ),
            ],
            return_type: Type::from_base_type(BaseType::Int(true)),
        };
        let mut body = vec![Stmt::Expr(Expr::Call(
            "srand".to_string(),
            vec![Expr::Call(
                "atoi".to_string(),
                vec![Expr::Deref(Box::new(Expr::Binop(
                    "+",
                    Box::new(Expr::Var("argv".to_string())),
                    Box::new(Expr::IConst(1)),
                )))],
            )],
        ))];
        for function in &self.functions {
            for (tpe, param) in &function.decl.params {
                body.push(Stmt::Decl(
                    Type::from_base_type(tpe.base_type),
                    format!("{}_{param}", function.decl.name),
                    Expr::Binop(
                        "%",
                        Box::new(Expr::Call("rand".to_string(), Vec::new())),
                        Box::new(Expr::IConst(128)),
                    ),
                ))
            }
            body.push(Stmt::Expr(Expr::Call(
                "printf".to_string(),
                vec![
                    Expr::StrLit(match function.decl.return_type.base_type {
                        BaseType::Float | BaseType::Double => "%f\\n".to_string(),
                        tpe if tpe.is_signed() => "%i\\n".to_string(),
                        _ => "%u\\n".to_string(),
                    }),
                    Expr::Call(
                        function.decl.name.to_string(),
                        function
                            .decl
                            .params
                            .iter()
                            .map(|(_, param)| {
                                Expr::Ref(Box::new(Expr::Var(format!(
                                    "{}_{param}",
                                    function.decl.name
                                ))))
                            })
                            .collect(),
                    ),
                ],
            )));
            for (tpe, param) in &function.decl.params {
                body.push(Stmt::Expr(Expr::Call(
                    "printf".to_string(),
                    vec![
                        Expr::StrLit(match tpe.base_type {
                            BaseType::Float | BaseType::Double => "%f\\n".to_string(),
                            tpe if tpe.is_signed() => "%i\\n".to_string(),
                            _ => "%u\\n".to_string(),
                        }),
                        Expr::Var(format!("{}_{param}", function.decl.name)),
                    ],
                )))
            }
        }
        Program {
            name: format!("{}_main", self.name),
            includes: vec![
                "stdio.h".to_string(),
                "stdlib.h".to_string(),
                format!("{}.c", self.name),
            ],
            functions: vec![Function { decl, body }],
        }
    }
}

#[derive(Debug)]
pub(crate) struct GenOptions {
    pub function_count: usize,
    pub param_count_range: RangeInclusive<usize>,
    pub block_size_range: RangeInclusive<usize>,
    pub max_stmt_depth: u32,
    pub max_expr_depth: u32,
}

#[derive(Debug)]
struct Function {
    decl: FunctionDecl,
    body: Vec<Stmt>,
}

impl Display for Function {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "{} {}({}){{{}}}",
            self.decl.return_type,
            self.decl.name,
            params_to_string(&self.decl.params),
            stmts_to_string(&self.body)
        )
    }
}

#[derive(Clone, Debug)]
struct FunctionDecl {
    name: String,
    params: Vec<(Type, String)>,
    return_type: Type,
}

#[derive(Debug)]
enum Stmt {
    Decl(Type, String, Expr),
    Assignment(Expr, Expr),
    Expr(Expr),
    IfStmt(Expr, Vec<Stmt>, Vec<Stmt>),
    WhileLoop(Box<Stmt>, Expr, Vec<Stmt>),
    ReturnStmt(Option<Expr>),
}

impl Display for Stmt {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Stmt::Decl(tpe, name, expr) => write!(f, "{tpe} {name}={expr}"),
            Stmt::Assignment(lhs, rhs) => write!(f, "{lhs}={rhs}"),
            Stmt::Expr(expr) => write!(f, "{expr}"),
            Stmt::IfStmt(cond, then, els) => write!(
                f,
                "if({cond}){{{}}}else{{{}}}",
                stmts_to_string(then),
                stmts_to_string(els)
            ),
            Stmt::WhileLoop(init, cond, body) => {
                write!(f, "{init};while({cond}){{{}}}", stmts_to_string(body))
            }
            Stmt::ReturnStmt(Some(expr)) => write!(f, "return {expr}"),
            Stmt::ReturnStmt(None) => write!(f, "return"),
        }
    }
}

#[derive(Debug)]
struct StmtsGen<'a> {
    function: &'a mut FunctionDecl,
    vars: &'a [(Type, String)],
    functions: &'a [FunctionDecl],
    options: &'a GenOptions,
    current_var_idx: usize,
    rng: &'a mut ThreadRng,
}

impl<'a> Gen for StmtsGen<'a> {
    fn rng(&mut self) -> &mut ThreadRng {
        self.rng
    }
}

impl<'a> StmtsGen<'a> {
    fn generate(
        function: &'a mut FunctionDecl,
        vars: &'a [(Type, String)],
        functions: &[FunctionDecl],
        options: &GenOptions,
        rng: &mut ThreadRng,
    ) -> Vec<Stmt> {
        let mut gen = StmtsGen {
            function,
            vars,
            functions,
            options,
            current_var_idx: 0,
            rng,
        };
        gen.generate_stmts(options.max_stmt_depth, true)
    }

    fn generate_stmts(&mut self, max_depth: u32, may_return: bool) -> Vec<Stmt> {
        let block_size = self.rng.gen_range(self.options.block_size_range.clone());
        let mut stmts = Vec::new();
        while stmts.len() < block_size {
            let may_return = may_return && stmts.len() + 1 == block_size;
            if let Some(stmt) = if may_return && max_depth == self.options.max_stmt_depth {
                self.generate_return_stmt()
            } else {
                self.generate_stmt(max_depth, may_return)
            } {
                stmts.push(stmt);
            }
        }
        stmts
    }

    fn generate_stmt(&mut self, max_depth: u32, may_return: bool) -> Option<Stmt> {
        let mut options: Vec<Box<dyn FnOnce(&mut StmtsGen) -> Option<Stmt>>> =
            vec![Box::new(|_self| _self.generate_assignment())];
        if may_return {
            options.push(Box::new(|_self| _self.generate_return_stmt()));
        }
        if max_depth > 0 {
            options.push(Box::new(move |_self| _self.generate_if_stmt(max_depth)));
            options.push(Box::new(move |_self| _self.generate_while_loop(max_depth)));
        }
        self.random_choice(options)
    }

    fn generate_assignment(&mut self) -> Option<Stmt> {
        self.generate_assignment_lhs().and_then(|(tpe, lhs)| {
            self.generate_rhs_until_not_lhs(&lhs, &|_self| {
                ExprGen::generate(
                    tpe,
                    _self.vars,
                    _self.functions,
                    _self.options.max_expr_depth,
                    _self.rng,
                )
            })
            .map(|rhs| Stmt::Assignment(lhs, rhs))
        })
    }

    fn generate_if_stmt(&mut self, max_depth: u32) -> Option<Stmt> {
        let cond = ExprGen::generate(
            Type::from_base_type(BaseType::Bool),
            self.vars,
            self.functions,
            self.options.max_expr_depth,
            self.rng,
        );
        cond.map(|cond| {
            let then = self.generate_stmts(max_depth - 1, true);
            let els = self.generate_stmts(max_depth - 1, false);
            Stmt::IfStmt(cond, then, els)
        })
    }

    fn generate_while_loop(&mut self, max_depth: u32) -> Option<Stmt> {
        let init_value = self.rng.gen_range(0..128);
        let cond_value = self.rng.gen_range(0..128);
        let init = Stmt::Decl(
            Type::from_base_type(BaseType::Int(true)),
            self.next_var_name(),
            Expr::IConst(init_value),
        );
        let mut body = Vec::new();
        let cond = if init_value < cond_value {
            body.push(Stmt::Assignment(
                Expr::Var(self.current_var_name()),
                Expr::Binop(
                    "+",
                    Box::new(Expr::Var(self.current_var_name())),
                    Box::new(Expr::IConst(1)),
                ),
            ));
            Expr::Binop(
                "<",
                Box::new(Expr::Var(self.current_var_name())),
                Box::new(Expr::IConst(cond_value)),
            )
        } else {
            body.push(Stmt::Assignment(
                Expr::Var(self.current_var_name()),
                Expr::Binop(
                    "-",
                    Box::new(Expr::Var(self.current_var_name())),
                    Box::new(Expr::IConst(1)),
                ),
            ));
            Expr::Binop(
                ">=",
                Box::new(Expr::Var(self.current_var_name())),
                Box::new(Expr::IConst(cond_value)),
            )
        };
        body.append(&mut self.generate_stmts(max_depth - 1, false));
        Some(Stmt::WhileLoop(Box::new(init), cond, body))
    }

    fn generate_return_stmt(&mut self) -> Option<Stmt> {
        ExprGen::generate(
            self.function.return_type,
            self.vars,
            self.functions,
            self.options.max_expr_depth,
            self.rng,
        )
        .map(|expr| Stmt::ReturnStmt(Some(expr)))
    }

    fn current_var_name(&mut self) -> String {
        format!("local{}", self.current_var_idx)
    }

    fn next_var_name(&mut self) -> String {
        self.current_var_idx += 1;
        self.current_var_name()
    }

    fn generate_assignment_lhs(&mut self) -> Option<(Type, Expr)> {
        (!self.vars.is_empty())
            .then(|| self.vars[self.rng.gen_range(0..self.vars.len())].clone())
            .map(|(tpe, var)| {
                let derefs = self.rng.gen_range(0..=tpe.pointer_depth);
                (
                    Type {
                        base_type: tpe.base_type,
                        pointer_depth: tpe.pointer_depth - derefs,
                    },
                    Expr::derefs(Expr::Var(var), derefs),
                )
            })
    }
}

#[derive(Debug, PartialEq)]
enum Expr {
    IConst(i32),
    FConst(f32),
    Bool(bool),
    StrLit(String),
    Unop(&'static str, Box<Expr>),
    Binop(&'static str, Box<Expr>, Box<Expr>),
    Select(Box<Expr>, Box<Expr>, Box<Expr>),
    Var(String),
    Ref(Box<Expr>),
    Deref(Box<Expr>),
    Call(String, Vec<Expr>),
    Cast(Type, Box<Expr>),
}

impl Display for Expr {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Expr::IConst(value) => write!(f, "{value}"),
            Expr::FConst(value) => write!(f, "{value}"),
            Expr::Bool(value) => write!(f, "{}", *value as i32),
            Expr::StrLit(value) => write!(f, "\"{value}\""),
            Expr::Unop(op, expr) => write!(f, "({op}{expr})"),
            Expr::Binop(op, lhs, rhs) => write!(f, "({lhs}{op}{rhs})"),
            Expr::Select(cond, lhs, rhs) => write!(f, "({cond}?{lhs}:{rhs})"),
            Expr::Var(name) => write!(f, "{name}"),
            Expr::Ref(expr) => write!(f, "&{expr}"),
            Expr::Deref(expr) => write!(f, "*{expr}"),
            Expr::Call(name, args) => {
                write!(f, "{name}({})", args_to_string(args))
            }
            Expr::Cast(tpe, expr) => write!(f, "({tpe}){expr}"),
        }
    }
}

impl Expr {
    fn derefs(expr: Expr, derefs: usize) -> Expr {
        if derefs > 0 {
            Expr::Deref(Box::new(Expr::derefs(expr, derefs - 1)))
        } else {
            expr
        }
    }
}

#[derive(Debug)]
struct ExprGen<'a> {
    vars: &'a [(Type, String)],
    functions: &'a [FunctionDecl],
    requires_var: bool,
    rng: &'a mut ThreadRng,
}

impl<'a> Gen for ExprGen<'a> {
    fn rng(&mut self) -> &mut ThreadRng {
        self.rng
    }
}

impl<'a> ExprGen<'a> {
    fn generate(
        tpe: Type,
        vars: &[(Type, String)],
        functions: &[FunctionDecl],
        max_depth: u32,
        rng: &mut ThreadRng,
    ) -> Option<Expr> {
        let mut gen = ExprGen {
            vars,
            functions,
            requires_var: true,
            rng,
        };
        gen.generate_expr(tpe, max_depth, true)
    }

    fn generate_expr(&mut self, tpe: Type, max_depth: u32, may_cast: bool) -> Option<Expr> {
        let mut options: Vec<Box<dyn FnOnce(&mut ExprGen) -> Option<Expr>>> =
            vec![Box::new(move |_self| _self.generate_var(tpe))];
        if max_depth > 0 {
            options.push(Box::new(move |_self| _self.generate_select(tpe, max_depth)));
            options.push(Box::new(move |_self| _self.generate_deref(tpe, max_depth)));
            options.push(Box::new(move |_self| _self.generate_call(tpe, max_depth)));
            if tpe.pointer_depth == 0 {
                options.push(Box::new(move |_self| _self.generate_op(tpe, max_depth)));
                if tpe.base_type == BaseType::Bool {
                    options.push(Box::new(move |_self| _self.generate_relop(max_depth)));
                } else if may_cast {
                    options.push(Box::new(move |_self| _self.generate_cast(tpe, max_depth)));
                }
            }
        }
        if tpe.pointer_depth == 0 && !self.requires_var {
            options.push(Box::new(move |_self| _self.generate_constant(tpe)));
        }
        self.random_choice(options)
    }

    fn generate_any_expr(
        &mut self,
        max_pointer_depth: usize,
        max_depth: u32,
        may_cast: bool,
    ) -> Option<(Type, Expr)> {
        #[allow(clippy::type_complexity)]
        let mut options: Vec<Box<dyn FnOnce(&mut ExprGen) -> Option<(Type, Expr)>>> = Vec::new();
        for base_type in [
            BaseType::Float,
            BaseType::Double,
            BaseType::Char(false),
            BaseType::Char(true),
            BaseType::Short(false),
            BaseType::Short(true),
            BaseType::Int(false),
            BaseType::Int(true),
            BaseType::LongLong(false),
            BaseType::LongLong(true),
            BaseType::Bool,
        ] {
            for pointer_depth in 0..=max_pointer_depth {
                options.push(Box::new(move |_self| {
                    let tpe = Type {
                        base_type,
                        pointer_depth,
                    };
                    _self
                        .generate_expr(tpe, max_depth, may_cast)
                        .map(|expr| (tpe, expr))
                }));
            }
        }
        self.random_choice(options)
    }

    fn generate_expr_or_ref(&mut self, tpe: Type, max_depth: u32) -> Option<Expr> {
        self.generate_expr(tpe, max_depth, true).or_else(|| {
            (tpe.pointer_depth > 0)
                .then(|| self.generate_var(tpe).map(|expr| Expr::Ref(Box::new(expr))))
                .flatten()
        })
    }

    fn generate_constant(&mut self, tpe: Type) -> Option<Expr> {
        match tpe.base_type {
            BaseType::Float | BaseType::Double => Some(Expr::FConst(self.rng.gen::<f32>() * 128.0)),
            BaseType::Char(false)
            | BaseType::Short(false)
            | BaseType::Int(false)
            | BaseType::LongLong(false)
            | BaseType::Char(true)
            | BaseType::Short(true)
            | BaseType::Int(true)
            | BaseType::LongLong(true) => Some(Expr::IConst(self.rng.gen_range(0..128))),
            BaseType::Bool => Some(Expr::Bool(self.rng.gen())),
        }
    }

    fn generate_op(&mut self, tpe: Type, max_depth: u32) -> Option<Expr> {
        self.requires_var = true;
        self.generate_expr(tpe, max_depth - 1, true)
            .and_then(|lhs| match tpe.base_type {
                BaseType::Float | BaseType::Double => self
                    .generate_rhs_until_not_lhs(&lhs, &|_self| _self.generate_rhs(tpe, max_depth))
                    .and_then(|rhs| match self.rng.gen_range(0..=3) {
                        0 => Some(Expr::Binop("+", Box::new(lhs), Box::new(rhs))),
                        1 => Some(Expr::Binop("-", Box::new(lhs), Box::new(rhs))),
                        2 => Some(Expr::Binop("*", Box::new(lhs), Box::new(rhs))),
                        3 => Some(Expr::Unop("-", Box::new(lhs))),
                        _ => unreachable!(),
                    }),
                BaseType::Char(_)
                | BaseType::Short(_)
                | BaseType::Int(_)
                | BaseType::LongLong(_) => self
                    .generate_rhs_until_not_lhs(&lhs, &|_self| _self.generate_rhs(tpe, max_depth))
                    .map(|rhs| {
                        let shift_amount =
                            self.rng.gen_range(0..tpe.base_type.size().min(32).into());
                        let mut options: Vec<Box<dyn FnOnce(Expr, Expr) -> Expr>> = vec![
                            Box::new(|lhs, rhs| Expr::Binop("+", Box::new(lhs), Box::new(rhs))),
                            Box::new(|lhs, rhs| Expr::Binop("-", Box::new(lhs), Box::new(rhs))),
                            Box::new(|lhs, rhs| Expr::Binop("*", Box::new(lhs), Box::new(rhs))),
                            Box::new(|lhs, rhs| Expr::Binop("&", Box::new(lhs), Box::new(rhs))),
                            Box::new(|lhs, rhs| Expr::Binop("|", Box::new(lhs), Box::new(rhs))),
                            Box::new(|lhs, rhs| Expr::Binop("^", Box::new(lhs), Box::new(rhs))),
                            Box::new(|lhs, _| {
                                Expr::Binop(
                                    "<<",
                                    Box::new(lhs),
                                    Box::new(Expr::IConst(shift_amount)),
                                )
                            }),
                            Box::new(|lhs, _| {
                                Expr::Binop(
                                    ">>",
                                    Box::new(lhs),
                                    Box::new(Expr::IConst(shift_amount)),
                                )
                            }),
                        ];
                        if tpe.base_type.is_signed() {
                            options.push(Box::new(|lhs, _| Expr::Unop("-", Box::new(lhs))));
                        }
                        options.remove(self.rng.gen_range(0..options.len()))(lhs, rhs)
                    }),
                BaseType::Bool => self
                    .generate_rhs_until_not_lhs(&lhs, &|_self| _self.generate_rhs(tpe, max_depth))
                    .and_then(|rhs| match self.rng.gen_range(0..=2) {
                        0 => Some(Expr::Binop("&&", Box::new(lhs), Box::new(rhs))),
                        1 => Some(Expr::Binop("||", Box::new(lhs), Box::new(rhs))),
                        2 => Some(Expr::Unop("!", Box::new(lhs))),
                        _ => unreachable!(),
                    }),
            })
    }

    fn generate_relop(&mut self, max_depth: u32) -> Option<Expr> {
        self.requires_var = true;
        self.generate_any_expr(0, max_depth - 1, true)
            .and_then(|(tpe, lhs)| {
                self.generate_rhs_until_not_lhs(&lhs, &|_self| _self.generate_rhs(tpe, max_depth))
                    .map(|rhs| match tpe.base_type {
                        BaseType::Float
                        | BaseType::Double
                        | BaseType::Char(_)
                        | BaseType::Short(_)
                        | BaseType::Int(_)
                        | BaseType::LongLong(_) => match self.rng.gen_range(0..=5) {
                            0 => Expr::Binop("==", Box::new(lhs), Box::new(rhs)),
                            1 => Expr::Binop("!=", Box::new(lhs), Box::new(rhs)),
                            2 => Expr::Binop("<", Box::new(lhs), Box::new(rhs)),
                            3 => Expr::Binop(">", Box::new(lhs), Box::new(rhs)),
                            4 => Expr::Binop("<=", Box::new(lhs), Box::new(rhs)),
                            5 => Expr::Binop(">=", Box::new(lhs), Box::new(rhs)),
                            _ => unreachable!(),
                        },
                        BaseType::Bool => match self.rng.gen_range(0..=1) {
                            0 => Expr::Binop("==", Box::new(lhs), Box::new(rhs)),
                            1 => Expr::Binop("!=", Box::new(lhs), Box::new(rhs)),
                            _ => unreachable!(),
                        },
                    })
            })
    }

    fn generate_select(&mut self, tpe: Type, max_depth: u32) -> Option<Expr> {
        self.requires_var = true;
        self.generate_expr(Type::from_base_type(BaseType::Bool), max_depth - 1, true)
            .and_then(|cond| {
                self.generate_expr(tpe, max_depth - 1, true)
                    .and_then(|lhs| {
                        self.generate_rhs_until_not_lhs(&lhs, &|_self| {
                            _self.generate_expr(tpe, max_depth - 1, true)
                        })
                        .map(|rhs| Expr::Select(Box::new(cond), Box::new(lhs), Box::new(rhs)))
                    })
            })
    }

    fn generate_var(&mut self, tpe: Type) -> Option<Expr> {
        let vars: Vec<_> = self
            .vars
            .iter()
            .filter_map(|(other_tpe, name)| (*other_tpe == tpe).then(|| name))
            .collect();
        (!vars.is_empty()).then(|| {
            self.requires_var = false;
            Expr::Var(vars[self.rng.gen_range(0..vars.len())].to_string())
        })
    }

    fn generate_deref(&mut self, tpe: Type, max_depth: u32) -> Option<Expr> {
        self.generate_expr(
            Type {
                base_type: tpe.base_type,
                pointer_depth: tpe.pointer_depth + 1,
            },
            max_depth - 1,
            false,
        )
        .map(|expr| Expr::Deref(Box::new(expr)))
    }

    fn generate_call(&mut self, tpe: Type, max_depth: u32) -> Option<Expr> {
        let functions: Vec<_> = self
            .functions
            .iter()
            .filter_map(|function| (function.return_type == tpe).then(|| function))
            .collect();
        (!functions.is_empty())
            .then(|| functions[self.rng.gen_range(0..functions.len())])
            .and_then(|function| {
                let args: Vec<_> = function
                    .params
                    .iter()
                    .filter_map(|(tpe, _)| self.generate_expr_or_ref(*tpe, max_depth - 1))
                    .collect();
                (args.len() == function.params.len()).then(|| {
                    self.requires_var = false;
                    Expr::Call(function.name.to_string(), args)
                })
            })
    }

    fn generate_cast(&mut self, tpe: Type, max_depth: u32) -> Option<Expr> {
        self.generate_any_expr(0, max_depth - 1, false)
            .map(|(other_tpe, expr)| {
                if other_tpe == tpe {
                    expr
                } else {
                    Expr::Cast(tpe, Box::new(expr))
                }
            })
    }

    fn generate_rhs(&mut self, tpe: Type, max_depth: u32) -> Option<Expr> {
        let mut options: Vec<Box<dyn FnOnce(&mut ExprGen) -> Option<Expr>>> =
            vec![Box::new(move |_self| {
                _self.generate_expr(tpe, max_depth - 1, true)
            })];
        match tpe.base_type {
            BaseType::Float | BaseType::Double => {
                for base_type in [
                    BaseType::Char(false),
                    BaseType::Char(true),
                    BaseType::Short(false),
                    BaseType::Short(true),
                    BaseType::Int(false),
                    BaseType::Int(true),
                ] {
                    options.push(Box::new(move |_self| {
                        _self.generate_expr(Type::from_base_type(base_type), max_depth - 1, true)
                    }))
                }
                if matches!(tpe.base_type, BaseType::Double) {
                    options.push(Box::new(move |_self| {
                        _self.generate_expr(
                            Type::from_base_type(BaseType::Float),
                            max_depth - 1,
                            true,
                        )
                    }))
                }
            }
            BaseType::Char(_) | BaseType::Short(_) | BaseType::Int(_) | BaseType::LongLong(_) => {
                for &base_type in [
                    BaseType::Char(false),
                    BaseType::Char(true),
                    BaseType::Short(false),
                    BaseType::Short(true),
                    BaseType::Int(false),
                    BaseType::Int(true),
                    BaseType::LongLong(false),
                    BaseType::LongLong(true),
                ]
                .iter()
                .filter(|rhs_base_type| rhs_base_type.size() <= tpe.base_type.size())
                {
                    options.push(Box::new(move |_self| {
                        _self.generate_expr(Type::from_base_type(base_type), max_depth - 1, true)
                    }))
                }
            }
            BaseType::Bool => {}
        }
        self.random_choice(options)
    }
}

trait Gen {
    fn rng(&mut self) -> &mut ThreadRng;

    fn random_choice<T>(
        &mut self,
        mut options: Vec<Box<dyn FnOnce(&mut Self) -> Option<T>>>,
    ) -> Option<T> {
        let mut choice = None;
        while choice.is_none() && !options.is_empty() {
            choice = options.remove(self.rng().gen_range(0..options.len()))(self)
        }
        choice
    }

    fn generate_rhs_until_not_lhs<T: PartialEq>(
        &mut self,
        lhs: &T,
        gen: &dyn Fn(&mut Self) -> Option<T>,
    ) -> Option<T> {
        let mut rhs = None;
        for _ in 0..MAX_TRIES {
            rhs = gen(self);
            if rhs.as_ref() != Some(lhs) {
                break;
            }
        }
        rhs.and_then(|rhs| (&rhs != lhs).then(|| rhs))
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
struct Type {
    base_type: BaseType,
    pointer_depth: usize,
}

impl Display for Type {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}{}", self.base_type, "*".repeat(self.pointer_depth))
    }
}

impl Type {
    fn from_base_type(base_type: BaseType) -> Type {
        Type {
            base_type,
            pointer_depth: 0,
        }
    }

    fn generate(pointer_depth: usize, rng: &mut ThreadRng) -> Type {
        let signed: bool = rng.gen();
        let options = [
            BaseType::Float,
            BaseType::Double,
            BaseType::Char(signed),
            BaseType::Short(signed),
            BaseType::Int(signed),
            BaseType::LongLong(signed),
            BaseType::Bool,
        ];
        Type {
            base_type: options[rng.gen_range(0..options.len())],
            pointer_depth,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum BaseType {
    Float,
    Double,
    Char(bool),
    Short(bool),
    Int(bool),
    LongLong(bool),
    Bool,
}

impl Display for BaseType {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            BaseType::Float => write!(f, "float"),
            BaseType::Double => write!(f, "double"),
            BaseType::Char(false) => write!(f, "unsigned char"),
            BaseType::Char(true) => write!(f, "char"),
            BaseType::Short(false) => write!(f, "unsigned short"),
            BaseType::Short(true) => write!(f, "short"),
            BaseType::Int(false) => write!(f, "unsigned"),
            BaseType::Int(true) => write!(f, "int"),
            BaseType::LongLong(false) => write!(f, "unsigned long long"),
            BaseType::LongLong(true) => write!(f, "long long"),
            BaseType::Bool => write!(f, "int"),
        }
    }
}

impl BaseType {
    fn size(self) -> u8 {
        match self {
            BaseType::Float => 32,
            BaseType::Double => 64,
            BaseType::Char(_) => 8,
            BaseType::Short(_) => 16,
            BaseType::Int(_) => 32,
            BaseType::LongLong(_) => 64,
            BaseType::Bool => 32,
        }
    }

    fn is_signed(self) -> bool {
        match self {
            BaseType::Float => true,
            BaseType::Double => true,
            BaseType::Char(signed) => signed,
            BaseType::Short(signed) => signed,
            BaseType::Int(signed) => signed,
            BaseType::LongLong(signed) => signed,
            BaseType::Bool => true,
        }
    }
}

fn includes_to_string(includes: &[String]) -> String {
    includes
        .iter()
        .map(|include| format!("#include \"{include}\"\n"))
        .collect::<Vec<_>>()
        .join("")
}

fn functions_to_string(functions: &[Function]) -> String {
    functions
        .iter()
        .map(Function::to_string)
        .collect::<Vec<_>>()
        .join("\n")
}

fn params_to_string(params: &[(Type, String)]) -> String {
    params
        .iter()
        .map(|(tpe, name)| format!("{tpe} {name}"))
        .collect::<Vec<_>>()
        .join(",")
}

fn stmts_to_string(stmts: &[Stmt]) -> String {
    stmts
        .iter()
        .map(|stmt| format!("{stmt};"))
        .collect::<Vec<_>>()
        .join("")
}

fn args_to_string(args: &[Expr]) -> String {
    args.iter()
        .map(Expr::to_string)
        .collect::<Vec<_>>()
        .join(",")
}
