#include "types.dl"


.functor binop(lhs: symbol, rhs: symbol, op: symbol):symbol stateful
.functor infix(lhs: symbol, rhs: symbol, op: symbol):symbol stateful
.functor type_code(type: symbol):symbol stateful
.functor trim_parens(sym: symbol):symbol stateful
.functor cat_semi(sym: symbol):symbol stateful


// Expressions
.decl _Expr(location: Location, expr: symbol)

// Numeric
_Expr([pos, pos], to_string(value)) :- IConst(pos, _, value).
_Expr([pos, pos], to_string(value)) :- FConst(pos, _, value).

// Unop
// Unimplemented Clz, Ctz, Popcnt, Nearest
_Expr([start, end], @infix("fabs(", ")", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end), Abs(end, _).

_Expr([start, end], @infix("(-", ")", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end), Neg(end, _).

_Expr([start, end], @infix("sqrt(", ")", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end), Sqrt(end, _).

_Expr([start, end], @infix("ceil(", ")", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end), Ceil(end, _).

_Expr([start, end], @infix("floor(", ")", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end), Floor(end, _).

_Expr([start, end], @infix("trunc(", ")", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end), FTrunc(end, _).

// Binop
// Unimplemented Rotl, Rotr
_Expr([start, end], @binop(lhs, rhs, "+")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Add(end, _).

_Expr([start, end], @binop(lhs, rhs, "-")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Sub(end, _).

_Expr([start, end], @binop(lhs, rhs, "*")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Mul(end, _).

_Expr([start, end], @binop(lhs, rhs, "/ ")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    (IDiv(end, _, _) ; FDiv(end, _)).

_Expr([start, end], @binop(lhs, rhs, "%")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Rem(end, _, _).

_Expr([start, end], @binop(lhs, rhs, "&")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), And(end, _).

_Expr([start, end], @binop(lhs, rhs, "|")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Or(end, _).

_Expr([start, end], @binop(lhs, rhs, "^")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Xor(end, _).

_Expr([start, end], @binop(lhs, rhs, "<<")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Shl(end, _).

_Expr([start, end], @binop(lhs, rhs, ">>")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Shr(end, _, _).

_Expr([start, end], cat("fmin", @binop(lhs, rhs, ","))) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Min(end, _).

_Expr([start, end], cat("fmax", @binop(lhs, rhs, ","))) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Max(end, _).

_Expr([start, end], cat("copysign", @binop(lhs, rhs, ","))) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    Copysign(end, _).

// Testop
_Expr([start, end], @infix("(!", ")", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end), Eqz(end).

// Relop
_Expr([start, end], @binop(lhs, rhs, "==")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Eq(end).

_Expr([start, end], @binop(lhs, rhs, "!=")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end), Ne(end).

_Expr([start, end], @binop(lhs, rhs, "<")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    (ILt(end, _) ; FLt(end)).

_Expr([start, end], @binop(lhs, rhs, ">")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    (IGt(end, _) ; FGt(end)).

_Expr([start, end], @binop(lhs, rhs, "<=")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    (ILe(end, _) ; FLe(end)).

_Expr([start, end], @binop(lhs, rhs, ">=")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    (IGe(end, _) ; FGe(end)).

// Cvtop
// Unimplemented TruncSat, Reinterpret
_Expr([start, end], expr) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    (Extend8(end, _) ; Extend16(end, _) ; Extend32(end, _) ; ExtendI32(end, _) ; Wrap(end) ;
    ITrunc(end, _, _, _) ; Demote(end) ; Promote(end) ; Convert(end, _, _, _)).

// Reference
// Unimplemented Func
_Expr([pos, pos], "NULL") :- Null(pos, _).

_Expr([start, end], @binop(expr, "NULL", "==")) :-
    _Expr([start, P1], expr), NextPos(P1, end), IsNull(end).

// Parametric
_Expr([start, end], expr) :- _Expr([start, P1], expr), NextPos(P1, end), Drop(end).

_Expr([start, end], @binop(cond, @infix(lhs, rhs, ":"), "?")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs),
    NextPos(P3, P4), _Expr([P4, P5], cond), NextPos(P5, end),
    (Select(end) ; TypedSelect(end, _)).

// Variable
// Unimplemented GlobalGet, GlobalSet
_Expr([pos, pos], name) :- LocalGet(pos, local), LocalNameAt(pos, local, name).

_Expr([start, end], @binop(name, @trim_parens(expr), "=")) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    (LocalSet(end, local) ; LocalTee(end, local)),
    LocalNameAt(end, local, name), FunctionLocals(_, _, local, _, name).

_Expr([start, end], name) :-
    _Expr([start, P1], _), NextPos(P1, end),
    (LocalSet(end, local) ; LocalTee(end, local)),
    LocalNameAt(end, local, name), FunctionLocals(_, _, local, _, other_name), ! other_name = name.

// Unimplemented Table

// Memory
// Unimplemented MemorySize, MemoryGrow, MemoryInit, DataDrop
_Expr([start, end], cat("*", @binop(expr, to_string(offset / align), "+"))) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    (Load(end, _, offset, align) ; Load8(end, _, _, offset, align) ;
    Load16(end, _, _, offset, align) ; Load32(end, _, _, offset, align)),
    ! (offset / align = 0).

_Expr([start, end], cat("*", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    (Load(end, _, offset, align) ; Load8(end, _, _, offset, align) ;
    Load16(end, _, _, offset, align) ; Load32(end, _, _, offset, align)),
    offset / align = 0.

_Expr([start, end], @binop(cat("*", @binop(lhs, to_string(offset / align), "+")),
        @trim_parens(rhs), "=")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    (Store(end, _, offset, align) ; Store8(end, _, offset, align) ;
    Store16(end, _, offset, align), Store32(end, _, offset, align)),
    ! (offset / align = 0).

_Expr([start, end], @binop(cat("*", lhs), @trim_parens(rhs), "=")) :-
    _Expr([start, P1], lhs), NextPos(P1, P2), _Expr([P2, P3], rhs), NextPos(P3, end),
    (Store(end, _, offset, align) ; Store8(end, _, offset, align) ;
    Store16(end, _, offset, align), Store32(end, _, offset, align)),
    offset / align = 0.

_Expr([start, end], cat("memset", @binop(dest, @binop(value, n, ","), ","))) :-
    _Expr([start, P1], dest), NextPos(P1, P2), _Expr([P2, P3], value),
    NextPos(P3, P4), _Expr([P4, P5], n), NextPos(P5, end),
    MemoryFill(end).

_Expr([start, end], cat("memcpy", @binop(dest, @binop(src, n, ","), ","))) :-
    _Expr([start, P1], dest), NextPos(P1, P2), _Expr([P2, P3], src),
    NextPos(P3, P4), _Expr([P4, P5], n), NextPos(P5, end),
    MemoryCopy(end).

// Control
// Unimplemented Nop, Unreachable, If, CallIndirect
_Expr([pos, pos], @infix("{", "}", block)) :-
    BlockInstr(pos, id), CBlock([id, 0], block),
    ! (Br(_, id) ; BrIf(_, id) ; BrTable(_, id) ; BrTableLabels(_, _, id)).

_Expr([pos, pos], @infix(@infix("{", "}", block), ":", label)) :-
    BlockInstr(pos, id), CBlock([id, 0], block),
    (Br(_, id) ; BrIf(_, id) ; BrTable(_, id) ; BrTableLabels(_, _, id)),
    Label(pos, label).

_Expr([pos, pos], cat("while(1)", @infix("{", "break;}", block))) :-
    Loop(pos, id), CBlock([id, 0], block),
    PosExists(br_pos), br_pos = [br_id, _idx], ! br_id = id,
    ! (Br(br_pos, id) ; BrIf(br_pos, id) ; BrTable(br_pos, id) ; BrTableLabels(br_pos, _, id)),
    ! HasLoopCondition(id).

_Expr([pos, pos], @infix(label, @infix("{", "break;}", block), ":while(1)")) :-
    Loop(pos, id), CBlock([id, 0], block),
    br_pos = [br_id, _], ! br_id = id,
    (Br(br_pos, id) ; BrIf(br_pos, id) ; BrTable(br_pos, id) ; BrTableLabels(br_pos, _, id)),
    Label(pos, label), ! HasLoopCondition(id).

_Expr([pos, pos], cat(@infix("while(", ")", condition), @infix("{", "}", block))) :-
    Loop(pos, id), CBlock([id, 0], block),
    PosExists(br_pos), br_pos = [br_id, _idx], ! br_id = id,
    ! (Br(br_pos, id) ; BrIf(br_pos, id) ; BrTable(br_pos, id) ; BrTableLabels(br_pos, _, id)),
    LoopCondition(id, condition).

_Expr([pos, pos], @infix(label, @infix("{", "}", block), @infix(":while(", ")", condition))) :-
    Loop(pos, id), CBlock([id, 0], block),
    br_pos = [br_id, _], ! br_id = id,
    (Br(br_pos, id) ; BrIf(br_pos, id) ; BrTable(br_pos, id) ; BrTableLabels(br_pos, _, id)),
    Label(pos, label), LoopCondition(id, condition).

_Expr([pos, pos], cat("goto ", label)) :-
    Br(pos, block),
    (BlockInstr(outer, block)
    ; pos = [other_block, _idx], ! other_block = block,
    Loop(outer, block)), Label(outer, label).

_Expr([start, end], cat(@infix("if(", ")", expr), @infix("goto ", ";", label))) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    BrIf(end, block),
    (BlockInstr(outer, block)
    ; end = [other_block, _idx], ! other_block = block,
    Loop(outer, block)), Label(outer, label).

_Expr([pos, pos], "continue") :-
    pos = [block, _idx], Br(pos, block), Loop(_, block), NextPos(pos, _).

_Expr([pos, pos], " ") :-
    pos = [block, _idx], Br(pos, block),
    (BlockInstr(_, block) ; Loop(_, block)), ! NextPos(pos, _).

_Expr([start, end], cat(@infix("if(", ")", expr), "continue;")) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    end = [block, _idx], BrIf(end, block), Loop(_, block), NextPos(end, _).
    
_Expr([start, end], " ") :-
    _Expr([start, P1], expr), NextPos(P1, end),
    end = [block, _idx], BrIf(end, block),
    (BlockInstr(_, block) ; Loop(_, block)), ! NextPos(end, _).

_Expr([start, end], @infix(@infix("switch(", "){", expr), "}", cases)) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    BrTable(end, _), Cases(end, 0, cases).

_Expr([start, end], cat("return ", expr)) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    end = [[function, id], _idx], Return(end),
    (NextPos(end, _) ; ! id = 0),
    Function(function, type, _), TypeResults(type, _, _).

_Expr([pos, pos], "return") :-
    pos = [[function, id], _idx], Return(pos),
    (NextPos(pos, _) ; ! id = 0),
    Function(function, type, _), ! TypeResults(type, _, _).

_Expr([start, end], expr) :-
    _Expr([start, P1], expr), NextPos(P1, end), Return(end),
    ! NextPos(end, _), end = [[_function, 0], _idx].

_Expr([start, end], cat(name, @infix("(", ")", args))) :-
    Call(end, function), Function(function, type, name),
    max_idx = max idx : TypeParams(type, idx, _),
    FunctionArgs([start, end], type, max_idx, args).


.decl Expr(location: Location, expr: symbol)

Expr(location, @trim_parens(expr)) :- _Expr(location, expr).

Expr(location, @infix(@infix(name, @trim_parens(val), "="), @trim_parens(expr), ";")) :-
    _Expr(location, expr), HasPos(location, pos), LocalTypeAt(pos, local, type),
    (LocalSet(pos, local) ; LocalTee(pos, local)),
    _Expr([_, P1], val), NextPos(P1, pos),
    LocalNameAt(pos, local, name), FunctionLocals(_, _, local, _, other_name),
    ! name = other_name.


.decl Label(pos: Pos, label: symbol)
Label(pos, cat("label", @infix(to_string(id), to_string(idx), "_"))) :-
    PosExists(pos), pos = [[_function, id], idx].


.decl _LocalNameAt(pos: Pos, local: Local, name: symbol)

_LocalNameAt(pos, local, @infix(name, @type_code(type), "_")) :-
    LocalTypeAt(pos, local, type), LocalType(local, other_type), ! type = other_type,
    PosExists(other_pos), (LocalSet(other_pos, local) ; LocalTee(other_pos, local)),
    LocalTypeAt(other_pos, local, type),
    FunctionLocals(_, _, local, _, name).

_LocalNameAt(pos, local, name) :- PosExists(pos), FunctionLocals(_, _, local, _, name).

.decl LocalNameAt(pos: Pos, local: Local, name: symbol)
LocalNameAt(pos, local, name) :-
    _LocalNameAt(pos, local, name), larger = count : {
        _LocalNameAt(pos, local, larger_name), strlen(larger_name) > strlen(name)
    }, larger = 0.


.decl LoopCondition(block: Block, condition: symbol)
LoopCondition(block, "1") :- pos = [block, _idx], Br(pos, block), ! NextPos(pos, _).
LoopCondition(block, expr) :- 
    _Expr([_, P1], expr), NextPos(P1, end),
    end = [block, _idx], BrIf(end, block), ! NextPos(end, _).


.decl HasLoopCondition(block: Block)
HasLoopCondition(block) :-
    pos = [block, _idx], (Br(pos, block) ; BrIf(pos, block)), ! NextPos(pos, _).


.decl Cases(pos: Pos, idx: unsigned, cases: symbol)

Cases(pos, idx, @infix("case ", @infix(":goto ", ";break;", label), to_string(idx))) :-
    BrTableLabels(pos, idx, block), (BlockInstr(outer, block) ; Loop(outer, block)),
    Label(outer, label), ! BrTableLabels(pos, idx + 1, _).

Cases(pos, idx, cat(@infix("case ", @infix(":goto ", ";break;", label), to_string(idx)), cases)) :-
    BrTableLabels(pos, idx, block), (BlockInstr(outer, block) ; Loop(outer, block)),
    Label(outer, label), Cases(pos, idx + 1, cases).

Cases(pos, idx, @infix("case ", ":continue;break;", to_string(idx))) :-
    pos = [block, _idx], BrTableLabels(pos, idx, block), Loop(_, block),
    ! BrTableLabels(pos, idx + 1, _).

Cases(pos, idx, cat(@infix("case ", ":continue;break;", to_string(idx)), cases)) :-
    pos = [block, _idx], BrTableLabels(pos, idx, block), Loop(_, block),
    Cases(pos, idx + 1, cases).


.decl FunctionArgs(location: Location, type: Type, idx: unsigned, args: symbol)

FunctionArgs([start, end], type, idx, expr) :-
    _Expr([start, P1], expr), NextPos(P1, end),
    TypeParams(type, idx, _), ! TypeParams(type, idx - 1, _).

FunctionArgs([start, end], type, idx, @infix(args, expr, ",")) :-
    _Expr([last, P1], expr), NextPos(P1, end),
    TypeParams(type, idx, _), FunctionArgs([start, last], type, idx - 1, args).


// Functions
.decl CBlock(pos: Pos, block: symbol)

CBlock([id, idx], @cat_semi(expr)) :-
    id = [_function, block], ! (block = 0),
    Expr([[id, idx], [id, end_idx]], expr), ! NextPos([id, end_idx], _).

CBlock([id, idx], @cat_semi(expr)) :-
    id = [function, 0], Function(function, type, _), ! TypeResults(type, _, _),
    Expr([[id, idx], [id, end_idx]], expr), ! NextPos([id, end_idx], _).

CBlock([id, idx], @cat_semi(@infix("return", expr, " "))) :-
    id = [function, 0], Function(function, type, _), TypeResults(type, _, _),
    Expr([[id, idx], [id, end_idx]], expr), ! NextPos([id, end_idx], _).

CBlock([id, idx], cat(@cat_semi(expr), block)) :-
    Expr([[id, idx], [id, end_idx]], expr), NextPos([id, end_idx], next),
    CBlock(next, block).


.decl FunctionParamsDecl(function: FunctionIndex, idx: unsigned, params: symbol)

FunctionParamsDecl(function, idx, @infix(ctype, name, " ")) :-
    Function(function, type, _), TypeParams(type, idx, _),
    LocalType(local, ctype), ! TypeParams(type, idx + 1, _),
    FunctionLocals(function, idx, local, _, name).

FunctionParamsDecl(function, idx, @infix(@infix(ctype, name, " "), params, ",")) :-
    Function(function, type, _), TypeParams(type, idx, _),
    LocalType(local, ctype), FunctionParamsDecl(function, idx + 1, params),
    FunctionLocals(function, idx, local, _, name).


.decl FunctionDecl(function: FunctionIndex, decl: symbol)
FunctionDecl(function, cat(@infix(ret_type, name, " "), @infix("(", ")", params))) :-
    Function(function, _, name), FunctionReturnType(function, ret_type),
    FunctionParamsDecl(function, 0, params).


.decl FunctionLocalsDecl(function: FunctionIndex, idx: unsigned, locals: symbol)

FunctionLocalsDecl(function, idx, cat(@infix(ctype, name, " "), "=0;")) :-
    Function(function, type, _), FunctionLocals(function, idx, local, _, name),
    LocalType(local, ctype), ! FunctionLocals(function, idx + 1, _, _, _),
    ! TypeParams(type, idx, _).

FunctionLocalsDecl(function, idx, " ") :-
    Function(function, type, _), ! FunctionLocals(function, idx + 1, _, _, _),
    TypeParams(type, idx, _).

FunctionLocalsDecl(function, idx, cat(cat(@infix(ctype, name, " "), "=0;"), locals)) :-
    Function(function, type, _), FunctionLocals(function, idx, local, _, name),
    LocalType(local, ctype), FunctionLocalsDecl(function, idx + 1, locals),
    ! TypeParams(type, idx, _).

FunctionLocalsDecl(function, idx, locals) :-
    Function(function, type, _), FunctionLocalsDecl(function, idx + 1, locals),
    TypeParams(type, idx, _).


.decl RenamedLocalsDecl(pos: Pos, locals: symbol)

RenamedLocalsDecl(pos, cat(@infix(type, name, " "), "=0;")) :-
    (LocalSet(pos, local) ; LocalTee(pos, local)),
    LocalNameAt(pos, local, name), FunctionLocals(_, _, local, _, other_name),
    ! name = other_name,
    LocalTypeAt(pos, local, type),
    earlier_set = count : {
        LaterPos(earlier_pos, pos),
        LocalSet(earlier_pos, local),
        LocalNameAt(earlier_pos, local, name)
    }, earlier_set = 0,
    earlier_tee = count : {
        LaterPos(earlier_pos, pos),
        LocalTee(earlier_pos, local),
        LocalNameAt(earlier_pos, local, name)
    }, earlier_tee = 0.

RenamedLocalsDecl(pos, cat(cat(@infix(type, name, " "), "=0;"), locals)) :-
    (LocalSet(pos, local) ; LocalTee(pos, local)),
    LocalNameAt(pos, local, name), FunctionLocals(_, _, local, _, other_name),
    ! name = other_name,
    LocalTypeAt(pos, local, type),
    earlier_set = count : {
        LaterPos(earlier_pos, pos),
        LocalSet(earlier_pos, local),
        LocalNameAt(earlier_pos, local, name)
    }, earlier_set = 0,
    earlier_tee = count : {
        LaterPos(earlier_pos, pos),
        LocalTee(earlier_pos, local),
        LocalNameAt(earlier_pos, local, name)
    }, earlier_tee = 0,
    LaterPos(pos, later_pos), RenamedLocalsDecl(later_pos, locals).

RenamedLocalsDecl(pos, locals) :-
    pos = [[_function, 0], 0],
    LaterPos(pos, later_pos), RenamedLocalsDecl(later_pos, locals).

RenamedLocalsDecl(pos, " ") :- pos = [[_function, 0], 0], PosExists(pos).


.decl CFunction(function: symbol)

CFunction(cat(decl, @infix("{", "}", @infix(locals, block, renamed_locals)))) :-
    pos = [[function, 0], 0], CBlock(pos, block), larger_blocks = count : {
        CBlock(pos, larger_block), strlen(larger_block) > strlen(block)
    }, larger_blocks = 0,
    FunctionDecl(function, decl), FunctionLocalsDecl(function, 0, locals),
    RenamedLocalsDecl(pos, renamed_locals), nlarger_renamed_locals = count : {
        RenamedLocalsDecl(pos, larger_renamed_locals),
        strlen(larger_renamed_locals) > strlen(renamed_locals)
    }, nlarger_renamed_locals = 0.

CFunction(cat(decl, "{}")) :-
    pos = [[function, 0], 0], ! CBlock(pos, _), FunctionDecl(function, decl).
.output CFunction
